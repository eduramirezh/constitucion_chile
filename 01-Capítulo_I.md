# Capítulo I: BASES DE LA INSTITUCIONALIDAD

### Artículo 1º

Los hombres nacen libres e iguales en dignidad y derechos.

La familia es el núcleo fundamental de la sociedad.

El Estado reconoce y ampara a los grupos intermedios a través de los cuales se organiza y estructura la sociedad y les garantiza la adecuada autonomía para cumplir sus propios fines específicos.

El Estado está al servicio de la persona humana y su finalidad es promover el bien común, para lo cual debe contribuir a crear las condiciones sociales que permitan a todos y a cada uno de los integrantes de la comunidad nacional su mayor realización espiritual y material posible, con pleno respeto a los derechos y garantías que esta Constitución establece.

Es deber del Estado resguardar la seguridad nacional, dar protección a la población y a la familia, propender al fortalecimiento de ésta, promover la integración armónica de todos los sectores de la Nación y asegurar el derecho de las personas a participar con igualdad de oportunidades en la vida nacional.

### Artículo 2º

Son emblemas nacionales la bandera nacional, el escudo de armas de la República y el himno nacional.

### Artículo 3º

El Estado de Chile es unitario, su territorio se divide en regiones. Su administración será funcional y territorialmente descentralizada, o desconcentrada en su caso, en conformidad con la ley.

### Artículo 4º

Chile es una república democrática.

### Artículo 5º

La soberanía reside esencialmente en la Nación. Su ejercicio se realiza por el pueblo a través del plebiscito y de elecciones periódicas y, también, por las autoridades que esta Constitución establece. Ningún sector del pueblo ni individuo alguno puede atribuirse su ejercicio.

El ejercicio de la soberanía reconoce como limitación el respeto a los derechos esenciales que emanan de la naturaleza humana. Es deber de los órganos del Estado respetar y promover tales derechos, garantizados por esta Constitución, así como los tratado s internacionales ratificados por Chile y que se encuentren vigentes.

### Artículo 6º

Los órganos del Estado deben someter su acción a la Constitución y a las normas dictadas conforme a ella.

Los preceptos de esta Constitución obligan tanto a los titulares o integrantes de dichos órganos como a toda persona, institución o grupo.

La infracción de esta norma generará las responsabilidades y sanciones que determine la ley.

### Artículo 7º

Los órganos del Estado actúan válidamente previa investidura regular de sus integrantes, dentro de su competencia y en la forma que prescriba la ley.

Ninguna magistratura, ninguna persona ni grupo de personas pueden atribuirse, ni aun a pretexto de circunstancias extraordinarias, otra autoridad o derechos que los que expresamente se les hayan conferido en virtud de la Constitución o las leyes.

Todo acto en contravención a este artículo es nulo y originará las responsabilidades y sanciones que la ley señale.

### Artículo 8º

Derogado.

### Artículo 9º

El terrorismo, en cualquiera de sus formas, es por esencia contrario a los derechos humanos.

Una ley de quórum calificado determinará las conductas terroristas y su penalidad. Los responsables de estos delitos quedarán inhabilitados por el plazo de quince años para ejercer funciones o cargos públicos, sean o no de elección popular, o de rector o director de establecimientos de educación, o para ejercer en ellos funciones de enseñanza; para explotar un medio de comunicación social o ser director o administrador del mismo, o para desempeñar en él funciones relacionadas con la emisión o difusión de opiniones o informaciones; ni podrán ser dirigentes de organizaciones políticas relacionadas con la educación o de carácter vecinal, profesional, empresarial, sindical, estudiantil o gremial en general, durante dicho plazo. Lo anterior se entiende sin perjuicio de otras inhabilidades o de las que por mayor tiempo establezca la ley.

Los delitos a que se refiere el inciso anterior serán considerados siempre
comunes y no políticos para todos los efectos legales y no procederá respecto de ellos el
indulto particular, salvo para conmutar la pena de muerte por la de presidio perpetuo.
