# Capítulo XI: CONSEJO DE SEGURIDAD NACIONAL

### Artículo 95

Habrá un Consejo de Seguridad Nacional, presidido por el Presidente de la República e integrado por los presidentes del Senado y de la Corte Suprema, por los Comandantes en Jefe de las Fuerzas Armadas, por el General Director de Carabineros y por el Contralor General de la República.

Participaran también como miembros del Consejo, con derecho a voz, los ministros encargados del gobierno interior, de las relaciones exteriores, de la defensa nacional y de la economía y finanzas del país. Actuara como Secretario el Jefe del Estado Mayor de la Defensa Nacional.

El Consejo de Seguridad Nacional podrá ser convocado por el Presidente de la República o a solicitud de dos de sus miembros y requerirá como quórum para sesionar el de la mayoría absoluta de sus integrantes. Para los efectos de la convocatoria al Consejo y del quórum para sesionar sólo se considerara a sus integrantes con derecho a voto. Los acuerdos se adoptarán por la mayoría absoluta de los miembros en ejercicio con derecho a voto.

### Artículo 96

Serán funciones del Consejo de Seguridad Nacional:

a) Asesorar al Presidente de la República en cualquier materia vinculada a la seguridad nacional en que éste lo solicite;

b) Hacer presente, al Presidente de la República, al Congreso Nacional o al Tribunal Constitucional, su opinión frente a algún hecho, acto o materia que, a su juicio, atente gravemente en contra de las bases de la institucionalidad o pueda comprometer la seguridad nacional;

c) Informar, previamente, respecto de las materias a que se refiere el número 14 del artículo 60;

d) Recabar de las autoridades y funcionarios de la administración todos los antecedentes relacionados con la seguridad exterior e interior del Estado. En tal caso, el requerido estará obligado a proporcionarlos y su negativa será sancionada en la forma que establezca la ley, y

e) Ejercer las demás atribuciones que esta Constitución le encomienda.

Los acuerdos u opiniones a que se refiere la letra b) serán públicos o reservados, según lo determine para cada caso particular el Consejo.

Un reglamento dictado por el propio Consejo establecerá las demás disposiciones concernientes a su organización y funcionamiento.
