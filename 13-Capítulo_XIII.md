# Capítulo XIII: GOBIERNO Y ADMINISTRACIÓN INTERIOR DEL ESTADO

### Artículo 99

Para el gobierno y administración interior del Estado, el territorio de la República se divide en regiones y estas en provincias. Para los efectos de la administración local, las provincias se dividirán en comunas.

La modificación de los límites de las regiones y la creación, modificación y supresión de las provincias y comunas, serán materia de ley de quórum calificado, como asimismo, la fijación de las capitales de las regiones y provincias; todo ello a proposición del Presidente de la República.

## Gobierno y Administración Regional

### Artículo 100

El Gobierno de cada región reside en un intendente que será de la exclusiva confianza del Presidente de la República. El intendente ejercerá sus funciones con arreglo a las leyes y a las órdenes e instrucciones del Presidente, de quien es su representante natural e inmediato en el territorio de su jurisdicción.

La administración superior de cada región radicará en un gobierno regional que tendrá por objeto el desarrollo social, cultural y económico de la región.

El gobierno regional estará constituido por el intendente y el consejo regional. Para el ejercicio de sus funciones, el gobierno regional gozará de personalidad jurídica de derecho público y tendrá patrimonio propio.

### Artículo 101

El intendente presidirá el consejo regional y le corresponderá la coordinación, supervigilancia o fiscalización de los servicios públicos creados por ley para el cumplimiento de las funciones administrativas que operen en la región.

La ley determinará la forma en que el intendente ejercerá estas facultades, las demás atribuciones que le correspondan y los organismos que colaborarán en el cumplimiento de sus funciones.

### Artículo 102

El consejo regional será un órgano de carácter normativo, resolutivo y fiscalizador, dentro del ámbito propio de competencia del gobierno regional, encargado de hacer efectiva la participación de la ciudadanía regional y ejercer las atribuciones que la ley orgánica constitucional respectiva le encomiende, la que regulará además su integración y organización.

Corresponderá desde luego al consejo regional aprobar los planes de desarrollo de la región y el proyecto de presupuesto del gobierno regional, ajustados a la política nacional de desarrollo y al presupuesto de la Nación. Asimismo, resolverá la inversión de los recursos consultados para la región en el fondo nacional de desarrollo regional, sobre la base de la propuesta que formule el intendente.

### Artículo 103

La ley deberá determinar las formas en que se descentralizará la administración del Estado, así como la transferencia de competencias a los gobiernos regionales.

Sin perjuicio de lo anterior, también establecerá, con las excepciones que procedan, la desconcentración regional de los ministerios y de los servicios públicos. Asimismo, regulará los procedimientos que aseguren la debida coordinación entre los órganos de la administración del Estado para facilitar el ejercicio de las facultades de las autoridades regionales.

### Artículo 104

Para el gobierno y administración interior del Estado a que se refiere el presente capítulo se observará como principio básico la búsqueda de un desarrollo territorial armónico y equitativo. Las leyes que se dicten al efecto deberán velar por el cumplimiento y aplicación de dicho principio, incorporando asimismo criterios de solidaridad entre las regiones, como al interior de ellas, en lo referente a la distribución de los recursos públicos.

Sin perjuicio de los recursos que para su funcionamiento se asignen a los gobiernos regionales en la Ley de Presupuestos de la Nación y de aquellos que provengan de lo dispuesto en el N° 20° del artículo 19, dicha ley contemplará una proporción del total de los gastos de inversión pública que determine, con la denominación de fondo nacional de desarrollo regional.

La Ley de Presupuestos de la Nación contemplará, asimismo, gastos correspondientes a inversiones sectoriales de asignación regional cuya distribución entre regiones responderá a criterios de equidad y eficiencia, tomando en consideración los programas nacionales de inversión correspondientes. La asignación de tales gastos al interior de cada región corresponderá al gobierno regional.

A iniciativa de los gobiernos regionales o de uno o más ministerios, podrán celebrarse convenios anuales o plurianuales de programación de inversión pública en la respectiva región o en el conjunto de regiones que convengan en asociarse con tal propósito.

La ley podrá autorizar a los gobiernos regionales y a las empresas públicas para asociarse con personas naturales o jurídicas a fin de propiciar actividades e iniciativas sin fines de lucro que contribuyan al desarrollo regional. Las entidades que al efecto se constituyan se regularán por las normas comunes aplicables a los particulares.

Lo dispuesto en el inciso anterior se entenderá sin perjuicio de lo establecido en el número 21° del artículo 19.

## Gobierno y Administración Provincial

### Artículo 105

En cada provincia existirá una gobernación que será un órgano territorialmente desconcentrado del intendente. Estará a cargo de un gobernador, quien será nombrado y removido libremente por el Presidente de la República.

Corresponde al gobernador ejercer, de acuerdo a las instrucciones del intendente, la supervigilancia de los servicios públicos existentes en la provincia. La ley determinará las atribuciones que podrá delegarle el intendente y las demás que le corresponden.

En cada provincia existirá un consejo económico y social provincial de carácter consultivo. La ley orgánica constitucional respectiva determinará su composición, forma de designación de sus integrantes, atribuciones y funcionamiento.

### Artículo 106

Los gobernadores, en los casos y forma que determine la ley, podrán designar delegados para el ejercicio de sus facultades en una o más localidades.

## Administración Comunal

### Artículo 107

La administración local de cada comuna o agrupación de comunas que determine la ley reside en una municipalidad, la que estará constituida por el alcalde, que es su máxima autoridad, y por el concejo. La ley orgánica establecerá un consejo económico y social comunal de carácter consultivo.

Las municipalidades son corporaciones autónomas de derecho público, con personalidad jurídica y patrimonio propio, cuya finalidad es satisfacer las necesidades de la comunidad local y asegurar su participación en el progreso económico, social y cultural de la comuna.

Una ley orgánica constitucional determinará las funciones y atribuciones de las municipalidades. Dicha ley señalará, además, las materias de administración municipal que el alcalde, con acuerdo del concejo o a requerimiento de la proporción de ciudadanos que establezca la ley, someterá a plebiscito, así como las oportunidades, forma de la convocatoria y efectos.

Las municipalidades podrán asociarse entre ellas para el cumplimiento de sus fines propios. Asimismo, podrán constituir corporaciones o fundaciones de derecho privado sin fines de lucro destinadas a la promoción y difusión del arte y la cultura. La participación municipal en ellas se regirá por la ley orgánica constitucional respectiva.

La municipalidades podrán establecer en el ámbito de las comunas o agrupación de comunas, de conformidad con la ley orgánica constitucional respectiva, territorios denominados unidades vecinales, con el objeto de propender a un desarrollo equilibrado y a una adecuada canalización de la participación ciudadana.

Los municipios y los demás servicios públicos existentes en la respectiva comuna deberán coordinar su acción en conformidad a la ley.

### Artículo 108

En cada municipalidad habrá un consejo integrado por concejales elegidos por sufragio universal en conformidad a la ley orgánica constitucional de municipalidades. Durarán cuatro años en sus cargos y podrán ser reelegidos. La misma ley determinará el número de concejales y la forma de elegir al alcalde.

El concejo será un órgano encargado de hacer efectiva la participación de la comunidad local, ejercerá funciones normativas, resolutivas y fiscalizadoras y otras atribuciones que se le encomienden, en la forma que determine la ley orgánica constitucional respectiva.

La ley orgánica de municipalidades determinará las normas sobre organización y funcionamiento del concejo y las materias en que la consulta del alcalde al concejo será obligatoria y aquellas en que necesariamente se requerirá el acuerdo de éste. En todo caso, será necesario dicho acuerdo para la aprobación del plan comunal de desarrollo, del presupuesto municipal y de los proyectos de inversión respectivos.

### Artículo 109

Los alcaldes, en los casos y formas que determine la ley orgánica constitucional respectiva, podrán designar delegados para el ejercicio de sus facultades en una o más localidades.

### Artículo 110

Derogado.

### Artículo 111

Las municipalidades gozarán de autonomía para la administración de sus finanzas. La Ley de Presupuestos de la Nación podrá asignarles recursos para atender sus gastos, sin perjuicio de los ingresos que directamente se les confieran por la ley o se les otorguen por los gobiernos regionales respectivos. Una ley orgánica constitucional contemplará un mecanismo de redistribución solidaria de los ingresos propios entre las municipalidades del país con la denominación de fondo común municipal. Las normas de distribución de este fondo serán materia de ley.

## Disposiciones Generales

### Artículo 112

La Ley establecerá fórmulas de coordinación para la administración de todos o algunos de los municipios, con respecto a los problemas que les sean comunes, así como entre los municipios y los demás servicios públicos.

### Artículo 113

Para ser designado intendente o gobernador y para ser elegido miembro del consejo regional o concejal, se requerirá ser ciudadano con derecho a sufragio, tener los demás requisitos de idoneidad que la ley señale y residir en la región a lo menos en los últimos dos años anteriores a su designación o elección.

Los cargos de intendente, gobernador, miembro del consejo regional y concejal serán incompatibles entre sí.

Ningún tribunal procederá criminalmente contra un intendente o gobernador sin que la Corte de Apelaciones respectiva haya declarado que ha lugar la formación de causa.

### Artículo 114

Las leyes orgánicas constitucionales respectivas establecerán las causales de cesación en los cargos de alcaldes, de miembro del consejo regional y de concejal.

### Artículo 115

La ley determinará la forma de resolver las cuestiones de competencia que pudieren suscitarse entre las autoridades nacionales, regionales, provinciales y comunales.

Asimismo, establecerá el modo de dirimir las discrepancias que se produzcan entre el intendente y el consejo regional, así como entre el alcalde y el concejo.
