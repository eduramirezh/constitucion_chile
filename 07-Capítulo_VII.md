# Capítulo VII: TRIBUNAL CONSTITUCIONAL

### Artículo 81

Habrá un Tribunal Constitucional integrado por siete miembros, designados en la siguiente forma:

a) Tres ministros de la Corte Suprema, elegidos por ésta, por mayoría absoluta, en votaciones sucesivas y secretas;

b) Un abogado designado por el Presidente de la República;

c) Dos abogados elegidos por el Consejo de Seguridad Nacional;

d) Un abogado elegido por el Senado, por mayoría absoluta de los senadores en ejercicio.

Las personas referidas en las letras b), c) y d) deberán tener a lo menos quince años de título, haberse destacado en la actividad profesional, universitaria o pública, no podrán tener impedimento alguno que las inhabilite para desempeñar el cargo de juez, estarán sometidas a las normas de los artículos 55 y 56, y sus cargos serán incompatibles con el de diputado o senador, así como también con la calidad de ministro del Tribunal Calificador de Elecciones. Además, en los casos de las letras b) y d), deberán ser personas que sean o hayan sido abogados integrantes de la Corte Suprema por tres años consecutivos, a lo menos.

Los miembros del Tribunal duraran ocho años en sus cargos, se renovaran por parcialidades cada cuatro años y serán inamovibles.

Les serán aplicables las disposiciones de los artículos 77, inciso segundo, en lo relativo a edad y el artículo 78.

Las personas a que se refiere la letra a) cesarán también en sus cargos si dejaren de ser ministros de la Corte Suprema por cualquier causa.

En caso de que un miembro del Tribunal Constitucional cese en su cargo, se procederá a su reemplazo por quien corresponda de acuerdo con el inciso primero de este artículo y por el tiempo que falte al reemplazado para completar su período.

El quórum para sesionar será de cinco miembros. El Tribunal adoptara sus acuerdos por simple mayoría y fallara con arreglo a derecho.

Una ley orgánica constitucional determinará la planta, remuneraciones y estatuto del personal del Tribunal Constitucional, así como su organización y funcionamiento.

### Artículo 82

Son atribuciones del Tribunal Constitucional:

1º.- Ejercer el control de la constitucionalidad de las leyes orgánicas constitucionales antes de su promulgación y de las leyes que interpreten algún precepto de la Constitución;

2º.- Resolver las cuestiones sobre constitucionalidad que se susciten durante la tramitación de los proyectos de ley o de reforma constitucional y de los tratados sometidos a la aprobación del Congreso;

3º.- Resolver las cuestiones que se susciten sobre la constitucionalidad de un decreto con fuerza de ley;

4º.- Resolver las cuestiones que se susciten sobre constitucionalidad con relación a la convocatoria a un plebiscito, sin perjuicio de las atribuciones que correspondan al Tribunal Calificador de Elecciones;

5º.- Resolver los reclamos en caso de que el Presidente de la República no promulgue una ley cuando deba hacerlo,promulgue un texto diverso del que constitucionalmente corresponda o dicte un decreto inconstitucional;

6º.- Resolver sobre la constitucionalidad de un decreto o resolución del Presidente de la República que la Contraloría haya representado por estimarlo inconstitucional, cuando sea requerido por el Presidente en conformidad al artículo 88;

7º.- Declarar la inconstitucionalidad de las organizaciones y de los movimientos o partidos políticos, como asimismo la responsabilidad de las personas que hubieren tenido participación en los hechos que motivaron la declaración de inconstitucionalidad, en conformidad a lo dispuesto en los incisos sexto, séptimo y octavo del número 15° del artículo 19 de esta Constitución. Sin embargo, si la persona afectada fuere el Presidente de la República o el Presidente electo, la referida declaración requerirá, además, el acuerdo del Senado adoptado por la mayoría de sus miembros en ejercicio;

8º.- Derogado

9º.- Informar al Senado en los casos a que se refiere el artículo 49 Nº. 7 de esta Constitución;

10º.- Resolver sobre las inhabilidades constitucionales o legales que afecten a una persona para ser designada Ministro de Estado, permanecer en dicho cargo o desempeñar simultáneamente otras funciones;

11º.- Pronunciarse sobre las inhabilidades, incompatibilidades y causales de cesación en el cargo de los parlamentarios, y

12º.- Resolver sobre la constitucionalidad de los decretos supremos dictados en el ejercicio de la potestad reglamentaria del Presidente de la República, cuando ellos se refieran a materias que pudieren estar reservadas a la ley por mandato del artículo 60.

El Tribunal Constitucional podrá apreciar en conciencia los hechos cuando conozca de las atribuciones indicadas en los números 7º., 9º. y 10º., como, asimismo, cuando conozca de las causales de cesación en el cargo de parlamentario. En el caso del numero 1º., la Cámara de origen enviara al Tribunal Constitucional el proyecto respectivo dentro de los cinco días siguientes a aquel en que quede totalmente tramitado por el Congreso.

En el caso del numero 2º., el Tribunal sólo podrá conocer de la materia a requerimiento del Presidente de la República, de cualquiera de las Cámaras o de una cuarta parte de sus miembros en ejercicio, siempre que sea formulado antes de la promulgación de la ley.

El Tribunal deberá resolver dentro del plazo de diez días contado desde que reciba el requerimiento, a menos que decida prorrogarlo hasta por otros diez días por motivos graves y calificados.

El requerimiento no suspenderá la tramitación del proyecto; pero la parte impugnada de éste no podrá ser promulgada hasta la expiración del plazo referido, salvo que se trate del proyecto de Ley de Presupuestos o del proyecto relativo a la declaración de guerra propuesta por el Presidente de la República.

En el caso del numero 3º., la cuestión podrá ser planteada por el Presidente de la República dentro del plazo de diez días cuando la Contraloría rechace por inconstitucional un decreto con fuerza de ley. También podrá ser promovida por cualquiera de las Cámaras o por una cuarta parte de sus miembros en ejercicio en caso de que la Contraloría hubiere tomado razón de un decreto con fuerza de ley que se impugne de inconstitucional. Este requerimiento deberá efectuarse dentro del plazo de treinta días, contados desde la publicación del respectivo decreto con fuerza de ley.

En el caso del numero 4°., la cuestión podrá promoverse a requerimiento del Senado o de la Cámara de Diputados, dentro de diez días contado desde la fecha de publicación del decreto que fije el día de la consulta plebiscitaria.

El Tribunal establecerá en su resolución el texto definitivo de la consulta plebiscitaria, cuando ésta fuere procedente.

Si al tiempo de dictarse la sentencia faltaren menos de treinta días para la realización del plebiscito, el Tribunal fijara en ella una nueva fecha comprendida entre los treinta y los sesenta días siguientes al fallo.

En los casos del numero 5º., la cuestión podrá promoverse por cualquiera de las Cámaras o por una cuarta parte de sus miembros en ejercicio, dentro de los treinta días siguientes a la publicación o notificación del texto impugnado o dentro de los sesenta días siguientes a la fecha en que el Presidente de la República debió efectuar la promulgación de la ley. Si el Tribunal acogiere el reclamo promulgará en su fallo la ley que no lo haya sido o rectificará la promulgación incorrecta.

En el caso del numero 9º., el Tribunal sólo podrá conocer de la materia a requerimiento de la Cámara de Diputados o de la cuarta parte de sus miembros en ejercicio.

Habrá acción pública para requerir al Tribunal respecto de las atribuciones que se le confieren por los números 7º. y 10º. de este artículo.

Sin embargo, si en el caso del numero 7º. la persona afectada fuere el Presidente de la República o el Presidente electo, el requerimiento deberá formularse por la Cámara de Diputados o por la cuarta parte de sus miembros en ejercicio.

En el caso del numero 11º., el Tribunal sólo podrá conocer de la materia a requerimiento del Presidente de la República o de no menos de diez parlamentarios en ejercicio.

En el caso del numero 12º., el Tribunal sólo podrá conocer de la materia a requerimiento de cualquiera de las Cámaras, efectuado dentro de los treinta días siguientes a la publicación o notificación del texto impugnado.

### Artículo 83

Contra las resoluciones del Tribunal Constitucional no procederá recurso alguno, sin perjuicio de que puede el mismo Tribunal, conforme a la ley, rectificar los errores de hecho en que hubiere incurrido.

Las disposiciones que el Tribunal declare inconstitucionales no podrán convertirse en ley en el proyecto o decreto con fuerza de ley de que se trate. En los casos de los números 5º. y 12º. del artículo 82, el decreto supremo impugnado quedara sin efecto de pleno derecho, con el sólo mérito de la sentencia del Tribunal que acoja el reclamo.

Resuelto por el Tribunal que un precepto legal determinado es constitucional, la Corte Suprema no podrá declararlo inaplicable por el mismo vicio que fue materia de la sentencia.
