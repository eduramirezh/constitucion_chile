# DISPOSICIONES TRANSITORIAS

### PRIMERA

Mientras se dictan las disposiciones que den cumplimiento a lo prescrito en el inciso tercero del numero 1º. del artículo 19 de esta Constitución, continuaran rigiendo los preceptos legales actualmente en vigor.

### SEGUNDA

Mientras se dicta el nuevo Código de Minería, que deberá regular, entre otras materias, la forma, condiciones y efectos de las concesiones mineras a que se refieren los incisos séptimo al décimo del numero 24º. del artículo 19 de esta Constitución Política, los titulares de derechos mineros seguirán regidos por la legislación que estuviere en vigor al momento en que entre en vigencia esta Constitución, en calidad de concesionarios.

Los derechos mineros a que se refiere el inciso anterior subsistirán bajo el imperio del nuevo Código, pero en cuanto a sus goces y cargas y en lo tocante a su extinción, prevalecerán las disposiciones de dicho nuevo Código de Minería. Este nuevo Código deberá otorgar plazo a los concesionarios para cumplir los nuevos requisitos que se establezcan para merecer amparo legal.

En el lapso que medie entre el momento en que se ponga en vigencia esta Constitución y aquel en que entre en vigor el nuevo Código de Minería, la constitución de derechos mineros con el carácter de concesión señalado en los incisos séptimo al décimo del numero 24º. del artículo 19 de esta Constitución, continuara regida por la legislación actual, al igual que las concesiones mismas que se otorguen.

### TERCERA

 La gran minería del cobre y las empresas consideradas como tal, nacionalizadas en virtud de lo prescrito en la disposición 17a. transitoria de la Constitución Política de 1925, continuaran rigiéndose por las normas constitucionales vigentes a la fecha de promulgación de esta Constitución.

### CUARTA

 La primera vez que se constituya el Tribunal Constitucional, los Ministros de la Corte Suprema a que se refiere la letra a) del artículo 81, que hayan sido elegidos en la segunda y tercera votación, y el abogado designado por el Presidente de la República a que se refiere la letra b) de dicho artículo, durarán cuatro años en sus cargos y los restantes, ocho años.

### QUINTA

 Se entenderá que las leyes actualmente en vigor sobre materias que conforme a esta Constitución deben ser objeto de leyes orgánicas constitucionales o aprobadas con quórum calificado, cumplen éstos requisitos y seguirán aplicándose en lo que no sean contrarias a la Constitución, mientras no se dicten los correspondientes cuerpos legales.

### SEXTA

 No obstante lo dispuesto en el numero 8º. del artículo 32, mantendrán su vigencia los preceptos legales que a la fecha de promulgación de esta Constitución hubieren reglado materias no comprendidas en el artículo 60, mientras ellas no sean expresamente derogadas por ley.

### SÉPTIMA

Sin perjuicio de lo dispuesto en el inciso tercero del numero 20º. del artículo 19, mantendrán su vigencia las disposiciones legales que hayan establecido tributos de afectación a un destino determinado, mientras no sean expresamente derogadas.

### OCTAVA

 Las normas relativas a la edad establecidas en el inciso segundo del artículo 77 no regirán respecto de los magistrados de los tribunales superiores de justicia en servicio a la fecha de vigencia de esta Constitución.

Durante el período a que se refiere la disposición decimotercera transitoria la inamovilidad de los Comandantes en Jefe de las Fuerzas Armadas y del General Director de Carabineros se regirá por la disposición transitoria vigésima y no les será aplicable la limitación del plazo contemplado en el artículo 93 de esta Constitución, el que se contara a partir de cuatro años del término del señalado período presidencial.

### NOVENA

 Los miembros del Tribunal Constitucional a que se refiere el artículo 81, deberán ser designados con diez días de anticipación, a lo menos, a la fecha en que comience el primer período presidencial. Para éste sólo efecto, el Consejo de Seguridad Nacional se constituirá con treinta días de anterioridad a la fecha en que comience a regir esta Constitución.

### DÉCIMA

En tanto no entre en vigencia la ley orgánica constitucional relativa a los partidos políticos a que se refiere el Nº. 15 del artículo 19, estará prohibido ejecutar o promover toda actividad, acción o gestión de índole político partidista, ya sea por personas naturales o jurídicas, organizaciones, entidades o agrupaciones de personas. Quienes infrinjan esta prohibición incurrirán en las sanciones previstas en la ley.

### DECIMOPRIMERA

 El artículo 84 de la Constitución relativo al Tribunal Calificador de Elecciones, comenzara a regir en la fecha que corresponda de acuerdo con la ley respectiva, con ocasión de la primera elección de senadores y diputados, y sus miembros deberán estar designados con treinta días de anticipación a esa fecha.

### DECIMOSEGUNDA

 Mientras no proceda constituir el Tribunal Calificador de Elecciones, la designación de los miembros de los tribunales electorales regionales, cuyo nombramiento le corresponde, será hecho por la Corte de Apelaciones respectiva.

### DECIMOTERCERA

 El período presidencial que comenzara a regir a contar de la vigencia de esta Constitución, durara el tiempo que establece el artículo 25.

Durante éste período serán aplicables todos los preceptos de la Constitución, con las modificaciones y salvedades que se indican en las disposiciones transitorias siguientes.

### DECIMOCUARTA

 Durante el período indicado en la disposición anterior, continuara como Presidente de la República el actual Presidente, General de Ejército don Augusto Pinochet Ugarte, quien durara en el cargo hasta el término de dicho período.

Asimismo, la Junta de Gobierno permanecerá integrada por los Comandantes en Jefe del Ejército, de la Armada y de la Fuerza Aérea, y por el General Director de Carabineros. Se regirá por las normas que regulen su funcionamiento interno y tendrá las atribuciones que se señalan en las disposiciones transitorias correspondientes.

Sin embargo, atendido que el Comandante en Jefe del Ejército, de acuerdo con el inciso primero de esta disposición es Presidente de la República, no integrará la Junta de Gobierno y lo hará, en su lugar, como miembro titular, el Oficial General de Armas del Ejército que le siga en antigüedad. Con todo, el Presidente de la República podrá reemplazar a dicho integrante en cualquier momento, por otro Oficial General de Armas de su Institución siguiendo el orden de antigüedad.

### DECIMOQUINTA

 El Presidente de la República tendrá las atribuciones y obligaciones que establecen los preceptos de esta Constitución, con las siguientes modificaciones y salvedades:

A.- Podrá:

1) Decretar por sí mismo los estados de emergencia y de catástrofe, en su caso, y

2) Designar y remover libremente a los alcaldes de todo el país, sin perjuicio de que pueda disponer la plena o gradual aplicación de lo previsto en el artículo 108.

B.- Requerirá el acuerdo de la Junta para:

1) Designar a los Comandantes en Jefe de las Fuerzas Armadas y al General Director de Carabineros cuando sea necesario reemplazarlos, por muerte, renuncia o cualquier clase de imposibilidad absoluta;

2) Designar al Contralor General de la República;

3) Declarar la guerra;

4) Decretar los estados de asamblea y de sitio;

5) Decidir si ha o no lugar a la admisión de las acusaciones que cualquier individuo particular presentare contra los Ministros de Estado con motivo de los perjuicios que pueda haber sufrido injustamente por algún acto cometido por éstos en el ejercicio de sus funciones, y

6) Ausentarse del país por más de treinta días o en los últimos noventa días de su período.

### DECIMOSEXTA

 En caso de que por impedimento temporal, ya sea por enfermedad, ausencia del territorio nacional u otro grave motivo, el Presidente de la República no pudiere ejercer su cargo, le subrogara con el título de Vicepresidente de la República, el miembro titular de la Junta de Gobierno según el orden de precedencia que corresponda.

### DECIMOSEPTIMA

 En caso de muerte, renuncia o cualquier clase de imposibilidad absoluta del Presidente de la República, el sucesor, por el período que le falte, será designado por la unanimidad de la Junta de Gobierno, la que deberá reunirse de inmediato. Mientras no se produzca la designación, asumirá como Vicepresidente de la República el miembro titular de la Junta de Gobierno, según el orden de precedencia que corresponda.

Si transcurridas cuarenta y ocho horas de reunida la Junta de Gobierno no hubiere unanimidad para elegir Presidente de la República, la elección la efectuara el Consejo de Seguridad Nacional, por la mayoría absoluta de sus miembros, integrándose a él, para éste efecto, el Contralor General de la República.

Si fuere designado Presidente de la República un Oficial General de Armas o de Orden y Seguridad, éste de pleno derecho y por el período presidencial que reste, asumirá la calidad de Comandante en Jefe Institucional o de General Director de Carabineros, en su caso, si tuviere los requisitos para serlo. En éste caso, el Oficial General de Armas o de Orden y Seguridad que le siga en antigüedad, en la respectiva Institución, pasara a integrar la Junta de Gobierno como miembro titular, aplicándose la parte final del inciso tercero de la disposición decimocuarta transitoria en cuanto a su Institución.

### DECIMOCTAVA

 Durante el período a que se refiere la disposición decimotercera transitoria, la Junta de Gobierno ejercerá, por la unanimidad de sus miembros, las siguientes atribuciones exclusivas:

A.- Ejercer el Poder Constituyente sujeto siempre a aprobación plebiscitaria, la que se llevara a efecto conforme a las reglas que señale la ley;

B.- Ejercer el Poder Legislativo;

C.- Dictar las leyes interpretativas de la Constitución que fueren necesarias;

D.- Aprobar o desechar los tratados internacionales, antes de la ratificación presidencial;

E.- Prestar su acuerdo al Presidente de la República en los casos contemplados en la letra B de la disposición decimoquinta transitoria;

F.- Prestar su acuerdo al Presidente de la República, para decretar los estados de asamblea y de sitio, en su caso;

G.- Permitir la entrada de tropas extranjeras en el territorio de la República, como asimismo, autorizar la salida de tropas nacionales fuera de él;

H.- Conocer de las contiendas de competencia que se susciten entre las autoridades políticas o administrativas y los tribunales superiores de justicia;

I.- Otorgar la rehabilitación de la ciudadanía, en los casos a que alude el artículo 17 numero 2º. de esta Constitución;

J.- Declarar en el caso de que el Presidente de la República o los Comandantes en Jefe de las Fuerzas Armadas y el General Director de Carabineros hicieren dimisión de su cargo, si los motivos que la originan son o no fundados y, en consecuencia, admitirla o desecharla, y

K.- Las demás que le otorgan otras disposiciones transitorias de esta Constitución.

El orden de precedencia de los integrantes de la Junta de Gobierno, es el que se indica a continuación:

1.- El Comandante en Jefe del Ejército;

2.- El Comandante en Jefe de la Armada;

3.- El Comandante en Jefe de la Fuerza Aérea, y

4.- El General Director de Carabineros.

Se alterara el orden de precedencia antes establecido, en las situaciones señaladas en el inciso tercero de la disposición decimocuarta transitoria y en el inciso final de la disposición decimoséptima transitoria, y, en tales casos, el integrante de la Junta de Gobierno a que aluden dichas disposiciones ocupara, como titular, el cuarto orden de precedencia.

Presidirá la Junta de Gobierno el miembro titular de ella que tenga el primer lugar de precedencia de acuerdo a los dos incisos anteriores.

En el caso previsto en la letra B.-, numero 1), de la disposición decimoquinta transitoria, el o los nuevos miembros que se incorporen a la Junta de Gobierno conservaran el orden de precedencia señalado en el inciso segundo.

Cuando uno de los miembros titulares de la Junta de Gobierno esté impedido temporalmente para ejercer su cargo, lo subrogara el Oficial General de Armas o de Orden y Seguridad más antiguo, a quien le corresponda de acuerdo a las normas sobre sucesión de mando en la respectiva Institución, integrándose a la Junta en el último lugar de precedencia. Si los subrogantes fueren más de uno, se integrarán a la Junta en el orden de precedencia señalado en el inciso segundo.

### DECIMONOVENA

 Los miembros de la Junta de Gobierno tendrán iniciativa de ley, en todas aquellas materias que constitucionalmente no sean de iniciativa exclusiva del Presidente de la República.

La Junta de Gobierno ejercerá mediante leyes las Potestades Constituyente y Legislativa. Estas leyes llevaran la firma de los miembros de la Junta de Gobierno y del Presidente de la República en señal de promulgación.

Una ley complementaria establecerá los órganos de trabajo y los procedimientos de que se valdrá la Junta de Gobierno, para ejercer las aludidas Potestades Constituyente y Legislativa. Estas normas complementarias establecerán, además, los mecanismos que permitan a la Junta de Gobierno requerir la colaboración de la comunidad para la elaboración de las leyes.

### VIGÉSIMA

En caso de duda acerca de si la imposibilidad que priva al Presidente de la República del ejercicio de sus funciones es de tal naturaleza que deba hacerse su reemplazo, corresponderá a los miembros titulares de la Junta de Gobierno resolver la duda planteada.

Si la duda se refiere a la imposibilidad que priva a un miembro de la Junta de Gobierno del ejercicio de sus funciones y es de igual naturaleza que la referida en el inciso anterior, corresponderá a los demás miembros titulares de la Junta de Gobierno resolver la cuestión planteada.

### VIGESIMAPRIMERA

 Durante el período a que se refiere la decimotercera disposición transitoria y hasta que entre en funciones el Senado y la Cámara de Diputados, no serán aplicables los siguientes preceptos de esta Constitución:

a) Los artículo 26 al 31 inclusive, los números 2º, 4º, 5º, 6º y la segunda parte del número 16º del artículo 32; el artículo 37; y el artículo 41, número 7º, en su referencia a los parlamentarios;

b) El Capítulo V sobre el Congreso Nacional con excepción de: el número 1º del artículo 50, los artículos 60, 61, los incisos tercero a quinto del artículo 62, y el artículo 64, los que tendrán plena vigencia. Las referencias que éstos preceptos y el número 3° del artículo 32, el inciso segundo del numero 6° del artículo 41, y los artículos 73 y 88 hacen al Congreso Nacional o a alguna de sus ramas, se entenderán hechas a la Junta de Gobierno.

Asimismo, la elección a que se refiere la letra d) del artículo 81, corresponderá hacerla a la Junta de Gobierno;

c) En el artículo 82: los números 4°, 9° y 11° de su inciso primero, el inciso segundo en su referencia al numero 9°, y los incisos octavo, noveno, décimo, decimosegundo, decimocuarto y decimoquinto. Tampoco regirá la referencia que el número 2° hace a la reforma constitucional, ni la segunda parte del número 8° del inciso primero del mismo artículo en lo atinente al Presidente de la República, como tampoco las referencias que hacen a dicho número, en lo concerniente a la materia, los incisos segundo y decimotercero;

d) El Capítulo XIV, relativo a la reforma de la Constitución. La Constitución sólo podrá ser modificada por la Junta de Gobierno en el ejercicio del Poder Constituyente. Sin embargo, para que las modificaciones tengan eficacia deberán ser aprobadas por plebiscito, el cual deberá ser convocado por el Presidente de la República, y

e) Cualquier otro precepto que sea contrario a las disposiciones que rigen el período presidencial a que se refiere la disposición decimotercera transitoria.

### VIGESIMASEGUNDA

 Para los efectos de lo prescrito en el inciso tercero del artículo 82 de la Constitución, la Junta de Gobierno deberá remitir al Tribunal Constitucional el proyecto a que dicho precepto se refiere, antes de su promulgación por el Presidente de la República.

Sin perjuicio de la facultad que se confiere al Presidente de la República en los incisos cuarto y séptimo del artículo 82, corresponderá también a la Junta de Gobierno en pleno formular el requerimiento a que aluden esas normas.

En el caso de los incisos decimoprimero y decimosexto del artículo señalado en el inciso anterior, corresponderá, asimismo, a la Junta de Gobierno en pleno formular el requerimiento respectivo.

### VIGESIMATERCERA

 Si entre la fecha de aprobación mediante plebiscito de la presente Constitución y la de su vigencia, el Presidente de la República a que se refiere la disposición decimocuarta transitoria quedare, por cualquier causa, impedido absolutamente de asumir sus funciones, la Junta de Gobierno, por la unanimidad de sus miembros, designara a la persona que asumirá el cargo de Presidente de la República para el período a que se refiere la disposición decimotercera transitoria.

Para éste efecto, la Junta de Gobierno se integrará por los Comandantes en Jefe de la Armada, de la Fuerza Aérea, por el General Director de Carabineros y, como miembro titular, por el Oficial General de Armas más antiguo del Ejército.

Si constituida la Junta de Gobierno en la forma indicada en el inciso precedente, no hubiere, dentro de las cuarenta y ocho horas de reunida, unanimidad para elegir Presidente de la República, se integrarán a ella, para éste sólo efecto, el Presidente de la Corte Suprema, el Contralor General de la República y el Presidente del Consejo de Estado y, así constituida, designara, por la mayoría absoluta de sus miembros, al Presidente de la República y a éste se entenderá referida la disposición decimocuarta transitoria, en su inciso primero.

### VIGESIMACUARTA

 Sin perjuicio de lo establecido en los artículos 39 y siguientes sobre estados de excepción que contempla esta Constitución, si durante el período a que se refiere la disposición decimotercera transitoria se produjeren actos de violencia destinados a alterar el orden público o hubiere peligro de perturbación de la paz interior, el Presidente de la República así lo declarara y tendrá, por seis meses renovables, las siguientes facultades:

a) Arrestar a personas hasta por el plazo de cinco días, en sus propias casas o en lugares que no sean cárceles. Si se produjeren actos terroristas de graves consecuencias, dicho plazo podrá extenderlo hasta por quince días más;

b) Restringir el derecho de reunión y la libertad de información, esta última sólo en cuanto a la fundación, edición o circulación de nuevas publicaciones;

c) Prohibir el ingreso al territorio nacional o expulsar de él a los que propaguen las doctrinas a que alude el artículo 8º. de la Constitución, a los que estén sindicados o tengan reputación de ser activistas de tales doctrinas y a los que realicen actos contrarios a los intereses de Chile o constituyan un peligro para la paz interior, y

d) Disponer la permanencia obligada de determinadas personas en una localidad urbana del territorio nacional hasta por un plazo no superior a tres meses.

Las facultades contempladas en esta disposición las ejercerá el Presidente de la República, mediante decreto supremo firmado por el Ministro del Interior, bajo la fórmula "Por orden del Presidente de la República". Las medidas que se adopten en virtud de esta disposición no serán susceptibles de recurso alguno, salvo el de reconsideración ante la autoridad que las dispuso.

### VIGESIMAQUINTA

 Durante el período a que se refiere la disposición decimotercera, el Consejo de Seguridad Nacional estará presidido por el Presidente de la República e integrado por los miembros de la Junta de Gobierno, por el Presidente de la Corte Suprema y por el Presidente del Consejo de Estado.

### VIGESIMASEXTA

 Hasta que el Senado entre en funciones continuara funcionando el Consejo de Estado.

### VIGESIMASÉPTIMA

Corresponderá a los Comandantes en Jefe de las Fuerzas Armadas y al General Director de Carabineros, titulares, proponer al país, por la unanimidad de ellos, sujeto a la ratificación de la ciudadanía, la persona que ocupara el cargo de Presidente de la República en el período presidencial siguiente al referido en la disposición decimotercera transitoria, quien deberá cumplir con los requisitos establecidos en el artículo 25 inciso primero de esta Constitución, sin que le sea aplicable la prohibición de ser reelegido contemplada en el inciso segundo de ese mismo artículo. Con ese objeto se reunirán noventa días antes, a lo menos, de la fecha en que deba cesar en el cargo el que esté en funciones. La designación será comunicada al Presidente de la República, para los efectos de la convocatoria a plebiscito.

Si transcurridas cuarenta y ocho horas de reunidos los Comandantes en Jefe y el General Director señalados en el inciso anterior, no hubiere unanimidad, la proposición se hará de acuerdo con lo prescrito en el inciso segundo de la disposición decimoséptima transitoria y el Consejo de Seguridad Nacional comunicara al Presidente de la República su decisión, para los mismos efectos señalados en el inciso anterior.

El plebiscito deberá efectuarse no antes de treinta ni después de sesenta días de la proposición correspondiente y se llevara a efecto en la forma que disponga la ley.

### VIGESIMAOCTAVA

 Si la ciudadanía a través del plebiscito manifestare su voluntad de aprobar la proposición efectuada de acuerdo con la disposición que precede, el Presidente de la República así elegido, asumirá el cargo el mismo día en que deba cesar el anterior y ejercerá sus funciones por el período indicado en el inciso segundo del artículo 25 y se aplicarán todos los preceptos de la Constitución con las siguientes modalidades:

A.- El Presidente de la República, nueve meses después de asumir el cargo, convocara a elecciones generales de senadores y diputados para integrar el Congreso en la forma dispuesta en la Constitución. La elección tendrá lugar no antes de los treinta ni después de los cuarenta y cinco días siguientes a la convocatoria y se efectuara de acuerdo a la ley orgánica respectiva;

B.- El Congreso Nacional se instalara tres meses después de la convocatoria a elecciones.

Los diputados de éste primer Congreso duraran tres años en sus cargos. Los senadores elegidos por las regiones de número impar durarán, asimismo, tres años y los senadores elegidos por las regiones de número par y región metropolitana, así como los designados, siete años, y

C.- Hasta que entre en funciones el Congreso Nacional, la Junta de Gobierno continuara en el pleno ejercicio de sus atribuciones, y seguirán en vigor las disposiciones transitorias que rigen el período presidencial a que se refiere la disposición decimotercera.

### VIGESIMANOVENA

Si la ciudadanía no aprobare la proposición sometida a plebiscito a que se refiere la disposición vigesimaséptima transitoria, se entenderá prorrogado de pleno derecho el período presidencial a que se refiere la disposición decimotercera transitoria, continuando en funciones por un año más el Presidente de la República en ejercicio y la Junta de Gobierno, con arreglo a las disposiciones que los rigen. Vencido éste plazo, tendrán plena vigencia todos los preceptos de la Constitución.

Para éste efecto, noventa días antes de la expiración de la prórroga indicada en el inciso anterior, el Presidente en ejercicio convocara a elección de Presidente de la República y de parlamentarios en conformidad a los preceptos permanentes de esta Constitución y de la ley.

El Presidente de la República que resulte elegido por aplicación del inciso anterior durará en el ejercicio de sus funciones por el término de cuatro años, y no podrá ser reelegido para el período inmediatamente siguiente.

### TRIGÉSIMA

En tanto no entre en vigencia la ley orgánica constitucional que determine las seis regiones en cada una de las cuales habrá dos circunscripciones senatoriales, se dividirán, en esta forma, las regiones de Valparaíso, Metropolitana de Santiago, del Maule, del Bío-Bío, de la Araucanía y de Los Lagos.

### TRIGESIMAPRIMERA

El indulto particular será siempre procedente respecto de los delitos a que se refiere el artículo 9° cometidos antes del 11 de Marzo de 1990. Una copia del Decreto respectivo se remitirá, en carácter reservado, al Senado.

### TRIGÉSIMASEGUNDA

Mientras no se proceda a la instalación de los gobiernos regionales que se establecen en esta Constitución, los intendentes y los consejos regionales de desarrollo mantendrán su actual composición y atribuciones, de conformidad a la legislación vigente.

### TRIGÉSIMATERCERA

Los alcaldes y consejos de desarrollo comunal continuarán en el desempeño de sus funciones, de conformidad con la legislación vigente, mientras no asuman los alcaldes y los concejales elegidos en virtud de esta reforma constitucional.

Las elecciones populares que se originen en esta reforma constitucional se efectuarán antes del 30 de junio de 1992. Las de los miembros de los consejos regionales, en la forma que prevea la ley orgánica constitucional respectiva, se celebrarán quince días después de la instalación de los concejos.

### TRIGÉSIMACUARTA

No obstante lo dispuesto en el inciso segundo del artículo 54, para las elecciones de diputados y senadores que corresponda realizar en 1993, no podrán ser candidatos los ciudadanos que resulten elegidos alcaldes, miembros de los consejos regionales o concejales en las elecciones que se celebren en 1992.
