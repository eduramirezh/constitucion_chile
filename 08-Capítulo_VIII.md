# Capítulo VIII: JUSTICIA ELECTORAL

### Artículo 84

Un tribunal especial, que se denominara Tribunal Calificador de Elecciones, conocerá del escrutinio general y de la calificación de las elecciones de Presidente de la República, de diputados y senadores; resolverá las reclamaciones a que dieren lugar y proclamara a los que resulten elegidos. Dicho Tribunal conocerá, asimismo, de los plebiscitos, y tendrá las demás atribuciones que determine la ley.

Estará constituido por cinco miembros designados en la siguiente forma:

a) Tres ministros o ex ministros de la Corte Suprema, elegidos por ésta en votaciones sucesivas y secretas, por la mayoría absoluta de sus miembros;

b) Un abogado elegido por la Corte Suprema en la forma señalada precedentemente y que reúna los requisitos que señala el inciso segundo del artículo 81;

c) Un ex presidente del Senado o de la Cámara de Diputados que haya ejercido el cargo por un lapso no inferior a tres años, el que será elegido por sorteo.

Las designaciones a que se refieren las letras b) y c) no podrán recaer en personas que sean parlamentario, candidato a cargos de elección popular, ministro de Estado, ni dirigente de partido político.

Los miembros de este tribunal duraran cuatro años en sus funciones y les serán aplicables las disposiciones de los artículo 55 y 56 de esta Constitución.

El Tribunal Calificador procederá como jurado en la apreciación de los hechos y sentenciara con arreglo a derecho.

Una ley orgánica constitucional regulara la organización y funcionamiento del Tribunal Calificador.

### Artículo 85

Habrá tribunales electorales regionales encargados de conocer el escrutinio general y la calificación de las elecciones que la ley les encomienda, así como de resolver las reclamaciones a que dieren lugar y de proclamar a los candidatos electos. Sus resoluciones serán apelables para ante el Tribunal Calificador de Elecciones en la forma que determine la ley. Asimismo, les corresponderá conocer de la calificación de las elecciones de carácter gremial y de las que tengan lugar en aquellos grupos intermedios que la ley señale.

Estos tribunales estarán constituidos por un ministro de la Corte de Apelaciones respectiva, elegido por ésta, y por dos miembros designados por el Tribunal Calificador de Elecciones de entre personas que hayan ejercido la profesión de abogado o desempeñado la función de ministro o abogado integrante de Corte de Apelaciones por un plazo no inferior a tres años.

Los miembros de éstos tribunales duraran cuatro años en sus funciones y tendrán las inhabilidades e incompatibilidades que determine la ley.

Estos tribunales procederán como jurado en la apreciación de los hechos y sentenciaran con arreglo a derecho.

La ley determinará las demás atribuciones de éstos tribunales y regulara su organización y funcionamiento.

### Artículo 86

Anualmente, se destinaran en la ley de Presupuesto de la Nación los fondos necesarios para la organización y funcionamiento de éstos tribunales, cuyas plantas, remuneraciones y estatuto del personal serán establecidos por ley.
