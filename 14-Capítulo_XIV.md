# Capítulo XIV: REFORMA DE LA CONSTITUCIÓN

### Artículo 116

Los proyectos de reforma de la Constitución podrán ser iniciados por mensaje del Presidente de la República o por moción de cualquiera de los miembros del Congreso Nacional, con las limitaciones señaladas en el inciso primero del artículo 62.

El proyecto de reforma necesitara para ser aprobado en cada Cámara el voto conforme de las tres quintas partes de los diputados y senadores en ejercicio. Si la reforma recayere sobre los capítulos I, III, VII, X, XI o XIV, necesitará, en cada Cámara, la aprobación de las dos terceras partes de los diputados y senadores en ejercicio.

Será aplicable a los proyectos de reforma constitucional el sistema de urgencias.

### Artículo 117

Las dos Cámaras, reunidas en Congreso Pleno y en sesión pública, con asistencia de la mayoría del total de sus miembros, sesenta días después de aprobado un proyecto en la forma señalada en el artículo anterior, tomaran conocimiento de él y procederán a votarlo sin debate.

Si en el día señalado no se reuniere la mayoría del total de los miembros del Congreso, la sesión se verificara al siguiente con los diputados y senadores que asistan.

El proyecto que apruebe la mayoría del Congreso Pleno pasara al Presidente de la República.

Si el Presidente de la República rechazare totalmente un proyecto de reforma aprobado por el Congreso y éste insistiere en su totalidad por las dos terceras partes de los miembros en ejercicio de cada Cámara, el Presidente deberá promulgar dicho proyecto, a menos que consulte a la ciudadanía mediante plebiscito.

Si el Presidente observare parcialmente un proyecto de reforma aprobado por el Congreso, las observaciones se entenderán aprobadas con el voto conforme de las tres quintas o dos terceras partes de los miembros en ejercicio de cada Cámara según corresponda, de acuerdo con el artículo anterior y se devolverá al Presidente para su promulgación.

En caso de que las Cámaras no aprueben todas o algunas de las observaciones del Presidente, no habrá reforma constitucional sobre los puntos en discrepancia, a menos que ambas Cámaras insistieren por los dos tercios de sus miembros en ejercicio en la parte del proyecto aprobado por ellas. En éste último caso, se devolverá al Presidente la parte del proyecto que haya sido objeto de insistencia para su promulgación, salvo que éste consulte a la ciudadanía para que se pronuncie mediante un plebiscito, respecto de las cuestiones en desacuerdo.

La ley orgánica constitucional relativa al Congreso regulara en lo demás lo concerniente a los vetos de los proyectos de reforma y a su tramitación en el Congreso.

### Artículo 118

Derogado.

### Artículo 119

La convocatoria a plebiscito deberá efectuarse dentro de los treinta días siguientes a aquel en que ambas Cámaras insistan en el proyecto aprobado por ellas, y se ordenara mediante decreto supremo que fijara la fecha de la votación plebiscitaria, la que no podrá tener lugar antes de treinta días ni después de sesenta, contado desde la publicación de dicho decreto. Transcurrido éste plazo sin que el Presidente convoque a plebiscito, se promulgará el proyecto que hubiere aprobado el Congreso.

El decreto de convocatoria contendrá, según corresponda, el proyecto aprobado por el Congreso Pleno y vetado totalmente por el Presidente de la República, o las cuestiones del proyecto en las cuales el Congreso haya insistido. En éste último caso, cada una de las cuestiones en desacuerdo deberá ser votada separadamente en el plebiscito.

El Tribunal Calificador comunicara al Presidente de la República el resultado del plebiscito, y especificara el texto del proyecto aprobado por la ciudadanía, el que deberá ser promulgado como reforma constitucional dentro de los cinco días siguientes a dicha comunicación.

Una vez promulgado el proyecto y desde la fecha de su vigencia, sus disposiciones formaran parte de la Constitución y se tendrán por incorporadas a ésta.

### Artículo final

La presente Constitución entrara en vigencia seis meses después de ser aprobada mediante plebiscito, con excepción de las disposiciones transitorias novena y vigesimatercera que tendrán vigor desde la fecha de esa aprobación. Su texto oficial será el que consta en éste decreto ley.

Un decreto ley determinará la oportunidad en la cual se efectuara el señalado plebiscito, así como las normas a que él se sujetara, debiendo establecer las reglas que aseguren el sufragio personal, igualitario y secreto y, para los nacionales, obligatorio.

La norma contenida en el inciso anterior entrara en vigencia desde la fecha de publicación del presente texto constitucional.
