# Capítulo II: NACIONALIDAD Y CIUDADANÍA

### Artículo 10

Son chilenos:

1º.- Los nacidos en el territorio de Chile, con excepción de los hijos de extranjeros que se encuentren en Chile en servicio de su Gobierno, y de los hijos de extranjeros transeúntes, todos los que, sin embargo, podrán optar por la nacionalidad chilena;

2º.- Los hijos de padre o madre chilenos nacidos en territorio extranjero, hallándose cualquiera de éstos en actual servicio de la República, quienes se considerarán para todos los efectos como nacidos en el territorio chileno;

3º.- Los hijos de padre o madre chilenos, nacidos en territorio extranjero, por el solo hecho de avecindarse por más de un año en Chile;

4º.- Los extranjeros que obtuvieren carta de nacionalización en conformidad a la ley, renunciando expresamente a su nacionalidad anterior. No se exigirá esta renuncia a los nacidos en país extranjero que, en virtud de un tratado internacional, conceda este mismo beneficio a los chilenos. Los nacionalizados en conformidad a este número tendrán opción a cargos públicos de elección popular sólo después de cinco años de estar en posesión de sus cartas de nacionalización, y

5º.- Los que obtuvieren especial gracia de nacionalización por ley.

La ley reglamentará los procedimientos de opción por la nacionalidad chilena; de otorgamiento, negativa y cancelación de las cartas de nacionalización, y la formación de un registro de todos estos actos.

### Artículo 11

La nacionalidad chilena se pierde:

1º.- Por nacionalización en país extranjero, salvo en el caso de aquellos chilenos comprendidos en los números 1º., 2º. y 3º. del artículo anterior que hubieren obtenido otra nacionalidad sin renunciar a su nacionalidad chilena y de acuerdo con lo establecido en el Nº. 4º. del mismo artículo.

La causal de pérdida de la nacionalidad chilena señalada precedentemente no regirá respecto de los chilenos que, en virtud de disposiciones constitucionales, legales o administrativas del Estado en cuyo territorio residan, adopten la nacionalidad extranjera como condición de su permanencia en él o de igualdad jurídica en el ejercicio de los derechos civiles con los nacionales del respectivo país;

2º.- Por decreto supremo, en caso de prestación de servicios durante una guerra exterior a enemigos de Chile o de sus aliados;

3º.- Por sentencia judicial condenatoria por delitos contra la dignidad de la patria o los intereses esenciales y permanentes del Estado, así considerados por ley aprobada con quórum calificado. En estos procesos, los hechos se apreciarán siempre en conciencia;

4º.- Por cancelación de la carta de nacionalización, y

5º.- Por ley que revoque la nacionalización concedida por gracia.

Los que hubieren perdido la nacionalidad chilena por cualquiera de las causales establecidas en este artículo, sólo podrán ser rehabilitados por ley.

### Artículo 12

La persona afectada por acto o resolución de autoridad administrativa que la prive de su nacionalidad chilena o se la desconozca, podrá recurrir, por sí o por cualquiera a su nombre, dentro del plazo de treinta días, ante la Corte Suprema, la que conocerá como jurado y en tribunal pleno. La interposición del recurso suspenderá los efectos del acto o resolución recurridos.

### Artículo 13

Son ciudadanos los chilenos que hayan cumplido dieciocho años de edad y que no hayan sido condenados a pena aflictiva.

La calidad de ciudadano otorga los derechos de sufragio, de optar a cargos de elección popular y los demás que la Constitución o la ley confieran.

### Artículo 14

Los extranjeros avecindados en Chile por más de cinco años, y que cumplan con los requisitos señalados en el inciso primero del artículo 13, podrán ejercer el derecho de sufragio en los casos y formas que determine la ley.

### Artículo 15

En las votaciones populares, el sufragio será personal, igualitario y secreto. Para los ciudadanos será, además, obligatorio.

Sólo podrá convocarse a votación popular para las elecciones y plebiscitos expresamente previstos en esta Constitución.

### Artículo 16

El derecho de sufragio se suspende:

1º.- Por interdicción en caso de demencia;

2º.- Por hallarse la persona procesada por delito que merezca pena aflictiva o por delito que la ley califique como conducta terrorista, y

3º.- Por haber sido sancionado por el Tribunal Constitucional en conformidad al inciso séptimo del artículo 19 de esta Constitución. Los que por esta causa se hallaren privados del ejercicio del derecho de sufragio lo recuperarán al término de cinco años, contado desde la declaración del Tribunal. Esta suspensión no producirá otro efecto legal, sin perjuicio de lo dispuesto en el inciso séptimo del número 15 del artículo 19.

### Artículo 17

La calidad de ciudadano se pierde:

1º.- Por pérdida de la nacionalidad chilena;

2º.- Por condena a pena aflictiva, y

3º.- Por condena por delitos que la ley califique como conducta terrorista.

Los que hubieren perdido la ciudadanía por la causal señalada en el numero 2º. podrán solicitar su rehabilitación al Senado, una vez extinguida su responsabilidad penal. Los que hubieren perdido la ciudadanía por la causal prevista en el numero 3º. sólo podrán ser rehabilitados en virtud de una ley de quórum calificado, una vez cumplida la condena.

### Artículo 18

Habrá un sistema electoral público. Una ley orgánica constitucional determinará su organización y funcionamiento, regulará la forma en que se realizarán los procesos electorales y plebiscitarios, en todo lo no previsto por esta Constitución y, garantizará siempre la plena igualdad entre los independientes y los miembros de partidos políticos tanto en la presentación de candidaturas como en su participación en los señalados procesos.

El resguardo del orden público durante los actos electorales y plebiscitarios corresponderá a las Fuerzas Armadas y Carabineros del modo que indique la ley.
