# Capítulo IV: GOBIERNO

## Presidente de la República

### Artículo 24

El gobierno y la administración del Estado corresponden al Presidente de la República, quien es el Jefe del Estado.

Su autoridad se extiende a todo cuanto tiene por objeto, la conservación del orden público en el interior y la seguridad externa de la República, de acuerdo con la Constitución y las leyes.

El Presidente de la República, a lo menos una vez al año, dará cuenta al país del estado administrativo y político de la nación.

### Artículo 25

Para ser elegido Presidente de la República se requiere haber nacido en el territorio de Chile, tener cumplidos cuarenta años de edad y poseer las demás calidades necesarias para ser ciudadano con derecho a sufragio.

El Presidente de la República durará en el ejercicio de sus funciones por el término de ocho años, y no podrá ser reelegido para el período siguiente.

El Presidente de la República no podrá salir del territorio nacional por más de treinta días ni en los últimos noventa días de su período, sin acuerdo del Senado.

En todo caso, el Presidente de la República comunicará con la debida anticipación al Senado su decisión de ausentarse del territorio y los motivos que la justifican.

### Artículo 26

El Presidente será elegido en votación directa y por mayoría absoluta de los sufragios válidamente emitidos. La elección se realizará, en la forma que determine la ley, noventa días antes de aquel en que deba cesar en el cargo el que esté en funciones.

Si la elección de Presidente se presentaren más de dos candidatos y ninguno de ellos obtuviere más de la mitad de los sufragios válidamente emitidos, se procederá a una nueva elección que se verificará, en la forma que determine la ley, quince días después de que el Tribunal Calificador, dentro del plazo señalado en el artículo siguiente, haga la la correspondiente declaración. Esta elección se circunscribirá a los dos candidatos que hubieren obtenido las más altas mayorías relativas.

Para los efectos de lo dispuesto en los dos incisos precedentes, los votos en blanco y los nulos se considerarán como no emitidos.

### Artículo 27

El proceso de calificación de la elección presidencial deberá quedar concluido dentro de los cuarenta días siguientes a la primera elección o de los veinticinco días siguientes a la segunda.

El Tribunal Calificador de Elecciones comunicará de inmediato al Presidente del Senado la proclamación de Presidente electo que haya efectuado.

El Congreso Pleno, reunido en sesión pública noventa días después de la primera o única elección y con los miembros que asistan, tomará conocimiento de la resolución en virtud de la cual el Tribunal Calificador proclama al Presidente electo.

En este mismo acto, el Presidente electo prestará ante el Presidente del Senado, juramento o promesa de desempeñar fielmente el cargo de Presidente de la República, conservar la independencia de la Nación, guardar y hacer guardar la Constitución y las leyes, y de inmediato asumirá sus funciones.

### Artículo 28

Si el Presidente electo se hallare impedido para tomar posesión del cargo, asumirá, mientras tanto, con el título de Vicepresidente de la República, el Presidente del Senado; a falta de éste, el Presidente de la Corte Suprema, y a falta de éste, el Presidente de la Cámara de Diputados.

Con todo, si el impedimento del Presidente electo fuere absoluto o debiere durar indefinidamente, el Vicepresidente, en los diez días siguientes al acuerdo del Senado adoptado en conformidad al artículo 49 N°. 7°., expedirá las órdenes convenientes para que se proceda, dentro del plazo de sesenta días, a nueva elección en la forma prevista por la Constitución y la Ley de Elecciones. El Presidente de la República así elegido asumirá sus funciones en la oportunidad que señale esa ley, y durará en el ejercicio de ellas hasta el día que le habría correspondido cesar en el cargo al electo que no pudo asumir y cuyo impedimento hubiere motivado la nueva elección.

### Artículo 29

Si por impedimento temporal, sea por enfermedad, ausencia del territorio u otro grave motivo, el Presidente de la República no pudiere ejercer su cargo, le subrogará, con el título de Vicepresidente de la República, el Ministro titular a quien corresponda de acuerdo con el orden de precedencia legal. A falta de éste, la subrogación corresponderá al Ministro titular que siga en ese orden de precedencia y, a falta de todos ellos, le subrogarán sucesivamente el Presidente del Senado, el Presidente de la Corte Suprema y el Presidente de la Cámara de Diputados.

En caso de vacancia del cargo de Presidente de la República, se producirá la subrogación como en las situaciones del inciso anterior, y se procederá a elegir sucesor en conformidad a las reglas de los incisos siguientes.

Si la vacancia se produjere faltando menos de dos años para la próxima elección general de parlamentarios, el Presidente será elegido por el Congreso Pleno por la mayoría absoluta de los senadores y diputados en ejercicio y durará en el cargo hasta noventa días después de esa elección general.

Conjuntamente, se efectuará una nueva elección presidencial por el período señalado en el inciso segundo del artículo 25. La elección por el Congreso será hecha dentro de los diez días siguientes a la fecha de la vacancia y el elegido asumirá su cargo dentro de los treinta días siguientes.

Si la vacancia se produjere faltando dos años o más para la próxima elección general de parlamentarios, el Vicepresidente, dentro de los diez primeros días de su mandato, convocará a los ciudadanos a elección presidencial para el nonagésimo día después de la convocatoria. El Presidente que resulte elegido asumirá su cargo el décimo día después de su proclamación y durará en él hasta noventa días después de la segunda elección general de parlamentarios que verifique durante su mandato, lo que se hará en conjunto con la nueva elección presidencial.

El Presidente elegido conforme a alguno de los incisos precedentes no podrá postular como candidato a la elección presidencial siguiente.

### Artículo 30

El Presidente cesará en su cargo el mismo día en que se complete su período y le sucederá el recientemente elegido.

### Artículo 31

El Presidente designado por el Congreso Pleno o, en su caso, el Vicepresidente de la República, tendrá todas las atribuciones que esta Constitución confiere al Presidente de la República.

### Artículo 32

Son atribuciones especiales del Presidente de la República:

1º.- Concurrir a la formación de las leyes con arreglo a la Constitución, sancionarlas y promulgarlas;

2º.- Convocar al Congreso a legislatura extraordinaria y clausurarla;

3º.- Dictar, previa delegación de facultades del Congreso, decretos con fuerza de ley sobre las materias que señala la Constitución;

4º.- Convocar a plebiscito en los casos del artículo 117;

5º.- Derogado.

6º.- Designar, en conformidad al artículo 45 de esta Constitución, a los integrantes del Senado que se indican en dicho precepto;

7º.- Declarar los estados de excepción constitucional en los casos y formas que se señalan en esta Constitución;

8º.- Ejercer la potestad reglamentaria en todas aquellas materias que no sean propia del dominio legal, sin perjuicio de la facultad de dictar los demás reglamentos, decretos e instrucciones que crea convenientes para la ejecución de las leyes;

9º.- Nombrar y remover a su voluntad a los ministros de Estado, subsecretarios, intendentes y gobernadores;

10º.- Designar a los embajadores y ministros diplomáticos, y a los representantes ante organismos internacionales. Tanto éstos funcionarios como los señalados en el Nº. 9º. precedente, serán de la confianza exclusiva del Presidente de la República y se mantendrán en sus puestos mientras cuenten con ella;

11º.- Nombrar al Contralor General de la República con acuerdo del Senado;

12º.- Nombrar y remover a los funcionarios que la ley denomina como de su exclusiva confianza y proveer los demás empleos civiles en conformidad a la ley. La remoción de los los demás funcionarios se hará de acuerdo a las disposiciones que ésta determine;

13º.- Conceder jubilaciones, retiros, montepíos y pensiones de gracia, con arreglo a las leyes;

14º.- Nombrar a los magistrados de los tribunales superiores de justicia y a los jueces letrados, a proposición de la Corte Suprema y de las Cortes de Apelaciones, respectivamente, y a los miembros del Tribunal Constitucional que le corresponda designar, todo ello conforme a lo prescrito en esta Constitución;

15º.- Velar por la conducta ministerial de los jueces y demás empleados del Poder Judicial y requerir, con tal objeto, a la Corte Suprema para que, si procede, declare su mal comportamiento o al ministerio público, para que reclame medidas disciplinarias del tribunal competente, o para que, si hubiere mérito bastante, entable la correspondiente acusación;

16º.- Otorgar indultos particulares en los casos y formas que determine la ley. El indulto será improcedente en tanto no se haya dictado sentencia ejecutoriada en el respectivo proceso. Los funcionarios acusados por la Cámara de Diputados y condenados por el Senado, sólo pueden ser ser indultados por el Congreso;

17º.- Conducir las relaciones políticas con las potencias extranjeras y organismos internacionales, y llevar a cabo las negociaciones; concluir, firmar y ratificar los tratados que estime convenientes para los intereses del país, los que deberán ser sometidos a la aprobación del Congreso conforme a lo prescrito en el artículo 50 Nº. 1º.. Las discusiones y deliberaciones sobre éstos objetos serán secretos si el Presidente de la República así lo exigiere;

18º.- Designar y remover a los Comandantes en Jefe del Ejército, de la Armada, de la Fuerza Aérea y al General Director de Carabineros en conformidad al artículo 93, y disponer los nombramientos, ascensos y retiros de los Oficiales de las Fuerzas Armadas y de Carabineros en la forma que señala el artículo 94;

19º.- Disponer de las fuerzas de aire, mar y tierra, organizarlas y distribuirlas de acuerdo con las necesidades de la seguridad nacional;

20º.- Asumir, en caso de guerra, la jefatura suprema de las Fuerzas Armadas;

21º.- Declarar la guerra, previa autorización por ley, debiendo dejar constancia de haber oído al Consejo de Seguridad Nacional, y

22º.- Cuidar de la recaudación de las rentas públicas y decretar su inversión con arreglo a la ley. El Presidente de la República, con la firma de todos los Ministros de Estado, podrá decretar pagos no autorizados por ley, para atender necesidades impostergables derivadas de calamidades públicas, de agresión exterior, de conmoción interna, de grave daño o o peligro para la seguridad nacional o del agotamiento de los recursos destinados a mantener servicios que no puedan paralizarse sin serio perjuicio para el país. El total de los giros que se hagan con éstos objetos no podrá exceder anualmente del dos por ciento (2%) del monto de los gastos que autorice la Ley de Presupuestos. Se podrá contratar empleados con cargo a esta misma ley, pero sin que el ítem respectivo pueda ser incrementado ni disminuido mediante traspasos. Los Ministros de Estado o funcionarios que autoricen o den curso a gastos que contravengan lo dispuesto en este número serán responsables solidaria y personalmente de su reintegro, y culpables del delito de malversación de caudales públicos.

## Ministros de Estado

### Artículo 33

Los Ministros de Estado son los colaboradores directos e inmediatos del Presidente de la República en el gobierno y administración del Estado.

La ley determinará el número y organización de los Ministerios, como también el orden de precedencia de los Ministros titulares.

El Presidente de la República podrá encomendar a uno o más Ministros la coordinación de la labor que corresponde a los Secretarios de Estado y las relaciones del Gobierno con el Congreso Nacional.

### Artículo 34

Para ser nombrado Ministro se requiere ser chileno, tener cumplidos veintiún años de edad y reunir los requisitos generales para el ingreso a la Administración Pública.

En los casos de ausencia, impedimento o renuncia de un Ministro, o cuando por otra causa se produzca la vacancia del cargo, será reemplazado en la forma que establezca la ley.

### Artículo 35

Los reglamentos y decretos del Presidente de la República deberán firmarse por el Ministro respectivo y no serán obedecidos sin este esencial requisito.

Los decretos e instrucciones podrán expedirse con la sola firma del Ministro respectivo, por orden del Presidente de la República, en conformidad a las normas que al efecto establezca la ley.

### Artículo 36

Los Ministros serán responsables individualmente de los actos que firmaren y solidariamente de los que suscribieren o acordaren con los otros Ministros.

### Artículo 37

Los Ministros podrán, cuando lo estimaren conveniente, asistir a las sesiones de la Cámara de Diputados o del Senado, y tomar parte en sus debates, con preferencia para hacer uso de la palabra, pero sin derecho a voto. Durante la votación podrán, sin embargo, rectificar los conceptos emitidos por cualquier diputado o senador al fundamentar su voto.

## Bases generales de la Administración del Estado

### Artículo 38

Una ley orgánica constitucional determinará la organización básica de la Administración Pública, garantizara la carrera funcionaria y los principios de carácter técnico y profesional en que deba fundarse, y asegurara tanto la igualdad de oportunidades de ingreso a ella como la capacitación y el perfeccionamiento de sus integrantes.

Cualquier persona que sea lesionada en sus derechos por la Administración del Estado, de sus organismos o de las municipalidades, podrá reclamar ante los tribunales que determine la ley, sin perjuicio de la responsabilidad que pudiere afectar al funcionario que hubiere causado el daño.

## Estados de excepción constitucional

### Artículo 39

El ejercicio de los derechos y garantías que la Constitución asegura a todas las personas sólo puede ser afectado en las siguientes situaciones de excepción: guerra externa o interna, conmoción interior, emergencia y calamidad pública.

### Artículo 40

1º.- En situación de guerra externa, el Presidente de la República, con acuerdo del Consejo de Seguridad Nacional, podrá declarar todo o parte del territorio nacional en estado de asamblea.

2º.- En caso de guerra interna o conmoción interior, el Presidente de la República podrá, con acuerdo del Congreso, declarar todo o parte del territorio nacional en estado de sitio.

El Congreso, dentro del plazo de diez días, contados desde la fecha en que el Presidente de la República someta la declaración de estado de sitio a su consideración, deberá pronunciarse aceptando o rechazando la proposición, sin que pueda introducirse modificaciones. Si el Congreso no se pronunciare dentro de dicho plazo, se entenderá que aprueba la proposición.

Sin embargo, el Presidente de la República, previo acuerdo del Consejo de Seguridad Nacional, podrá aplicar el estado de sitio de inmediato, mientras el Congreso se pronuncia sobre la declaración.

Cada rama del Congreso deberá emitir su pronunciamiento, por la mayoría de los miembros presentes, sobre la declaración de estado de sitio propuesta por el Presidente de la República. Podrá el Congreso, en cualquier tiempo y por la mayoría absoluta de los miembros en ejercicio de cada Cámara, dejar sin efecto el estado de sitio que hubiere aprobado.

La declaración de estado de sitio sólo podrá hacerse hasta por un plazo máximo de noventa días, pero el Presidente de la República podrá solicitar su prórroga, la que se tramitara en conformidad a las normas precedentes.

3º.- El Presidente de la República, con acuerdo del Consejo de Seguridad Nacional podrá declarar todo o parte del territorio nacional en estado de emergencia, en casos graves de alteración del orden público, daño o peligro para la seguridad nacional, sea por causa de origen interno o externo.

Dicho estado no podrá exceder de noventa días, pudiendo declararse nuevamente si se mantienen las circunstancias.

4º.- En caso de calamidad pública, el Presidente de la República, con acuerdo del Consejo de Seguridad Nacional, podrá declarar la zona afectada o cualquiera otra que lo requiera como consecuencia de la calamidad producida, en estado de catástrofe.

5º.- El Presidente de la República podrá decretar simultáneamente dos o más estados de excepción si concurren las causales que permiten su declaración.

6º.- El Presidente de la República podrá, en cualquier tiempo, poner término a dichos estados.

### Artículo 41

1º.- Por la declaración de estado de asamblea el Presidente de la República queda facultado para suspender o restringir la libertad personal, el derecho de reunión, la libertad de información y de opinión y la libertad de trabajo. Podrá, también, restringir el ejercicio del derecho de asociación y de sindicación, imponer censura a la correspondencia y a las comunicaciones, disponer requisiciones de bienes y establecer limitaciones al ejercicio del derecho de propiedad.

2º.- Por la declaración de estado de sitio, el Presidente de la República podrá trasladar a las personas de un punto a otro del territorio nacional, arrestarlas en sus propias casas o en lugares que no sean cárceles ni en otros que estén destinados a la detención o prisión de reos comunes. Podrá, además, suspender o restringir el ejercicio del derecho de reunión y restringir el ejercicio de las libertades de locomoción, de información y de opinión.
La medida de traslado deberá cumplirse en localidades urbanas que reúnan las condiciones que la ley determine.

3º.- Los tribunales de justicia no podrán, en caso alguno, entrar a calificar los fundamentos ni las circunstancias de hecho invocadas por la autoridad para adoptar las medidas en el ejercicio de las facultades excepcionales que le confiere esta Constitución. La interposición y tramitación de los recursos de amparo y de protección que conozcan los tribunales no suspenderán los efectos de las medidas decretadas, sin perjuicio de lo que resuelvan en definitiva respecto de tales recursos.

4º.- Por la declaración de estado de emergencia, se podrá restringir el ejercicio de la libertad de locomoción y del derecho de reunión.

5º.- Por la declaración del estado de catástrofe el Presidente de la República podrá restringir la circulación de las personas y el transporte de mercaderías, y las libertades de trabajo, de información y de opinión, y de reunión. Podrá, asimismo, disponer requisiciones de bienes y establecer limitaciones al ejercicio del derecho de propiedad, y adoptar todas las medidas extraordinarias de carácter administrativo que estime necesarias.

6º.- Declarado el estado de emergencia o de catástrofe, las zonas respectivas quedaran bajo la dependencia inmediata del jefe de la Defensa Nacional que el Gobierno designe, quien asumirá el mando con las atribuciones y deberes que la ley señale.

El Presidente de la República estará obligado a informar al Congreso de las medidas adoptadas en virtud de los estados de emergencia y de catástrofe.

7º.- Las medidas que se adopten durante los estados de excepción, no podrán prolongarse más allá de la vigencia de dichos estados. En ningún caso las medidas de restricción y privación de la libertad podrán adoptarse en contra de los parlamentarios, de los jueces, de los miembros del Tribunal Constitucional, del Contralor General de la República y de los miembros del Tribunal Calificador de Elecciones.

8º.- Las requisiciones que se practiquen darán lugar a indemnizaciones en conformidad a la ley. También darán derecho a indemnización las limitaciones que se impongan al derecho de propiedad cuando importen privación de alguno de los atributos o facultades esenciales del dominio, y con ello se cause daño.

9º.- Una ley orgánica constitucional podrá regular los estados de excepción y facultar al Presidente de la República para ejercer por sí o por otras autoridades las atribuciones señaladas precedentemente, sin perjuicio de lo establecido en los estados de emergencia y de catástrofe.
